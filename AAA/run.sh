#!/bin/sh

#ideally you need to set only the following two paths ANDROID_BUILD_DIR and SDK_DIR

#where you downloaded the android sources and built it
ANDROID_BUILD_DIR=/home/yuyifei/Desktop/taintmagic
SDK_DIR=/home/yuyifei/Desktop/taintmagic/out/host/linux-x86/sdk/android-sdk_eng.root_linux-x86/platforms/android-2.3.5

CUR_DIR=`pwd`
ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# copy various jars
#cd $ANDROID_BUILD_DIR
#cp out/target/common/obj/JAVA_LIBRARIES/bouncycastle_intermediates/classes.jar $ROOT_DIR/libs/bouncycastle.jar
#cp out/target/common/obj/JAVA_LIBRARIES/ext_intermediates/classes.jar $ROOT_DIR/libs/ext.jar
#cp out/target/common/obj/JAVA_LIBRARIES/core_intermediates/classes.jar $ROOT_DIR/libs/core.jar
#cp out/target/common/obj/JAVA_LIBRARIES/core-junit_intermediates/classes.jar $ROOT_DIR/libs/junit.jar
#cp out/target/common/obj/JAVA_LIBRARIES/framework_intermediates/classes.jar $ROOT_DIR/libs/framework.jar


#build the instrumented system.img
cd $ROOT_DIR 
ant -f run.xml 

# modify ramdisk.img
rm -rf ramdisk
mkdir ramdisk
cd ramdisk
cp $SDK_DIR/images/ramdisk.img .
mv ramdisk.img ramdisk.cpio.gz
gzip -d ramdisk.cpio.gz
mkdir tmp
cp ramdisk.cpio tmp
cd tmp
cpio -i -F ramdisk.cpio
rm ramdisk.cpio
cp $ROOT_DIR/tools/init.rc .
cpio -i -t -F ../ramdisk.cpio | cpio -o -H newc -O ../ramdisk_new.cpio
cd ..
gzip ramdisk_new.cpio
mv ramdisk_new.cpio.gz ramdisk_new.img
cp ramdisk_new.img $SDK_DIR/images/ramdisk.img

#run emulator
echo "run emulator ..."
cd $ANDROID_BUILD_DIR
source run-emulator.sh

cd $CUR_DIR
