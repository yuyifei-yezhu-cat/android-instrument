package com.yuyifei;

import java.util.Iterator;
import java.util.Map;

import soot.Body;
import soot.BodyTransformer;
import soot.G;
import soot.Local;
import soot.PackManager;
import soot.PatchingChain;
import soot.RefType;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.Transform;
import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.jimple.AbstractStmtSwitch;
import soot.jimple.IdentityStmt;
import soot.jimple.IntConstant;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.Jimple;
import soot.jimple.Ref;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;
import soot.jimple.StringConstant;
import soot.jimple.ThisRef;
import soot.jimple.VirtualInvokeExpr;
import soot.options.Options;
import soot.util.Chain;


public class AndroidInstrument {
	
	public static void main(String[] args) 
	{
		
		final String androidJARPath = "/home/yuyifei/Desktop/android-jar/android-platforms-master";
		final String androidJAR = "/home/yuyifei/Desktop/android-jar/android-platforms-master/android-17/android.jar"; //required for CH resolution
		final String apkPath = "/home/yuyifei/Desktop/githhub-paper/android-instrumentation-tutorial/app-example/RV2013/bin/RV2013.apk";
		String[] args1 = {
				//"-cp",
				//"/home/yuyifei/Downloads/yuyifei/BBB/bin",
				//":/home/yuyifei/Downloads/yuyifei/BBB/bin/com/yuyifei.MyPrint.class",
				//"-android-jars",
				//"/home/yuyifei/Desktop/android-jar/android-platforms-master",
				"-allow-phantom-refs",
				"-ire",
				"-process-dir",
				//"/home/yuyifei/Desktop/apk-test/SkeletonActivity.apk"
				apkPath
		};
		G.reset();
		//prefer Android APK files// -src-prec apk
		//Options.v().set_soot_classpath("/home/yuyifei/Desktop/android-jar/android-platforms-master:./src");
		Options.v().set_src_prec(Options.src_prec_apk);
		Options.v().set_force_android_jar(androidJAR);
		Options.v().set_soot_classpath(androidJAR + ":./src");
		//Scene.v().setSootClassPath("./libs/android-19.jar"+":"+libJars+":"+modelClasses + ":" + apk);
		
		//output as APK, too//-f J
		Options.v().set_output_format(Options.output_format_dex);
		//Options.v().set_output_format(Options.output_format_jimple);
		
        // resolve the PrintStream and System soot-classes
        Scene.v().addBasicClass("java.io.PrintStream",SootClass.SIGNATURES);
        Scene.v().addBasicClass("java.lang.System",SootClass.SIGNATURES);
        Scene.v().addBasicClass("android.widget.Toast", SootClass.SIGNATURES);
        Scene.v().addBasicClass("de.ecspride.RV2013", SootClass.SIGNATURES);
        Scene.v().addBasicClass("com.yuyifei.MyPrint", SootClass.SIGNATURES);
        Scene.v().addBasicClass("android.view.View", SootClass.SIGNATURES);
        Scene.v().addBasicClass("com.yuyifei.instrumentHelpClass", SootClass.SIGNATURES);
        
        PackManager.v().getPack("jtp").add(new Transform("jtp.myInstrumenter", new MyBodyTransformer()));
		
		soot.Main.main(args1);
	}


}

class MyBodyTransformer extends BodyTransformer
{
	/* some internal fields */
	SootClass helpClass;
	SootClass instrumentClass;
	
	@Override
	protected void internalTransform(final Body body, String phaseName, @SuppressWarnings("rawtypes") Map options)
	{
		helpClass = Scene.v().loadClassAndSupport("com.yuyifei.MyPrint");
		instrumentClass = Scene.v().loadClassAndSupport("com.yuyifei.instrumentHelpClass");
		
		if (body.getMethod().getDeclaringClass().getName().startsWith("de.ecspride")) 
		{
			final PatchingChain<Unit> units = body.getUnits();
			
			if (body.getMethod().getName().contains("sendSms"))
			{
				//add a this lcoal
		        // Add locals, java.io.printStream tmpRef
//		        Local arg = Jimple.v().newLocal("tmp_this", RefType.v("de.ecspride.RV2013"));
//		        body.getLocals().add(arg);
//		        
//		        //add a unit
//		        body.getUnits().insertAfter(Jimple.v().newIdentityStmt(arg, 
//	    		Jimple.v().newThisRef(RefType.v("de.ecspride.RV2013"))),
//	    		body.getUnits().getFirst());
		        
		        
//				SootMethod method = body.getMethod();
//				method.getDeclaringClass().getType();
//				Local p = Jimple.v().newLocal("pp", method.getDeclaringClass().getType());
//				body.getLocals().add(p);
//				IdentityStmt is = Jimple.v().newIdentityStmt(p, Jimple.v().newThisRef((RefType) p.getType()));
//				body.getUnits().insertAfter(is,
//						body.getUnits().getFirst());
		        
				Iterator<Unit> i = units.snapshotIterator();
				while (i.hasNext()) 
				{
					Unit u = i.next();
					if(u.toString().contains("println"))
					{
						//removeStatement(u, body);				
						//replaceStatementByLog(u, body);
						//instrumentAlertDialog(u, body);
						//testHelpClass(u, body);
						//instrumentHelp(u, body);
						//printParameter(u, body);
						//break;
					}
					//eliminatePremiumRateSMS(u, body);
					if(u instanceof InvokeStmt)
					{
						InvokeStmt invoke = (InvokeStmt)u;
						if(invoke.getInvokeExpr().getMethod().getSignature().equals("<android.telephony.SmsManager: void sendTextMessage(java.lang.String,java.lang.String,"
								+ "java.lang.String,android.app.PendingIntent,android.app.PendingIntent)>"))
						{
							//getMethodParameter(u, body);
							//getParameter(u, body);
							//getStack(u, body);
							instrumentAlertDialog(u, body);
						}
					}
				}
			}
		}
		
        //check that we did not mess up the Jimple
        //body.validate();
	}
	
	
	
	private void instrumentHelp(Unit u, Body body)
	{
		System.out.println("+++++++++++++ yuyifeiaa:" + instrumentClass.getMethodCount());
		
		SootMethod stackMethod =  instrumentClass.getMethod("void myPrintStack()");
		// 1. make invoke expression of MyCounter.report()
		InvokeExpr reportExpr= Jimple.v().newStaticInvokeExpr(stackMethod.makeRef());

		// 2. then, make a invoke statement
		Stmt reportStmt = Jimple.v().newInvokeStmt(reportExpr);

		// 3. insert new statement into the chain
		//    (we are mutating the unit chain).
		body.getUnits().insertBefore(reportStmt, u);
	}
	
	
	private void getStack(Unit u, Body body)
	{
		
	}
	
	private void getMethodParameter(Unit u, Body body)
	{
		Iterator<ValueBox> boxs = u.getUseBoxes().iterator();
		while (boxs.hasNext())
		{
			//System.out.println("-------->" + boxs.next().toString());
			System.out.println("--------->" + boxs.next().getValue().toString());
		}
	}
	
	private void printParameter(Unit u, Body body)
	{
		Value param = body.getParameterRefs().get(0);
		
		SootMethod printMethod =  instrumentClass.getMethodByName("printObject");
		System.out.println("---------->" + printMethod.getSignature());
		
		StaticInvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(printMethod.makeRef(), body.getParameterLocal(0));
		
		Unit generated = Jimple.v().newInvokeStmt(invokeExpr);
		
		body.getUnits().insertBefore(generated, u);
	}
	
	private void getParameter(Unit u, Body body)
	{
        Scene.v().addBasicClass("java.io.PrintStream",SootClass.SIGNATURES);
        Scene.v().addBasicClass("java.lang.System",SootClass.SIGNATURES);
		
		System.out.println("-----------> yuyifei:" + u.toString());
		SootMethod sm = body.getMethod();
		System.out.println("-----------> yuyifei:" + sm.getName());
		Iterator<Local> params = body.getParameterLocals().iterator();
		while (params.hasNext())
		{
			System.out.println("yuyifei:" + params.next().toString());
		}
		
//		Iterator<SootMethod> ss = Scene.v().getMethodNumberer().iterator();
//		while (ss.hasNext())
//		{
//			System.out.println("***** yuyifei:" + ss.next().toString());
//		}
		
		SootClass sc = Scene.v().getSootClass("android.view.View");
		System.out.println("---------------> " + sc.getName());
		Iterator<SootMethod> viewMethods = sc.getMethods().iterator();
		while (viewMethods.hasNext())
		{
			System.out.println(viewMethods.next().getSignature());
		}
		
		SootMethod toString = Scene.v().getMethod("<android.view.View: java.lang.String toString()>");
		VirtualInvokeExpr virInvoket = Jimple.v().newVirtualInvokeExpr(body.getParameterLocal(0), toString.makeRef());
		//body.getUnits().insertBefore(Jimple.v().newInvokeStmt(virInvoket), u);
		
		Local paramContennt = Jimple.v().newLocal("paramContennt", RefType.v("java.lang.String"));
		body.getLocals().add(paramContennt);
		body.getUnits().insertBefore(Jimple.v().newAssignStmt(paramContennt, virInvoket), u);
		
		SootMethod sm_log = Scene.v().getMethod("<android.util.Log: int i(java.lang.String,java.lang.String)>");
		Value logType = StringConstant.v("YUYIFEIss");
		Value logMessage = StringConstant.v("-------->:");
		StaticInvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(sm_log.makeRef(),
															logType, 
															//logMessage
															paramContennt
															);
		Unit generated = Jimple.v().newInvokeStmt(invokeExpr);
		
		body.getUnits().insertBefore(generated, u);
	}
	
	private void testHelpClass(Unit u, Body body)
	{
		System.out.println("+++++++++++++ yuyifei:" + helpClass.getMethodCount());
		Iterator<SootMethod> sms = helpClass.getMethods().iterator();
		while (sms.hasNext())
		{
			SootMethod sm = sms.next();
			System.out.println("+++++++++++ yuyifei:" + sm.getName());
//			// 1. make invoke expression of MyCounter.report()
//			InvokeExpr reportExpr= Jimple.v().newStaticInvokeExpr(reportCounter.makeRef());
//
//			// 2. then, make a invoke statement
//			Stmt reportStmt = Jimple.v().newInvokeStmt(reportExpr);
//
//			// 3. insert new statement into the chain
//			//    (we are mutating the unit chain).
//			units.insertBefore(reportStmt, stmt);
		}
		
		SootMethod printMethod = helpClass.getMethod("void print()");
		// 1. make invoke expression of MyCounter.report()
		InvokeExpr reportExpr= Jimple.v().newStaticInvokeExpr(printMethod.makeRef());

		// 2. then, make a invoke statement
		Stmt reportStmt = Jimple.v().newInvokeStmt(reportExpr);

		// 3. insert new statement into the chain
		//    (we are mutating the unit chain).
		body.getUnits().insertBefore(reportStmt, u);
	}//increaseCounter = counterClass.getMethod("void increase(int)");
	
	private void instrumentAlertDialog(Unit u, Body body)
	{
		SootMethod alertDialog = instrumentClass.getMethodByName("instrumentAlertDialog");
		
		System.out.println("------->" + alertDialog.getSignature());
		
		InvokeExpr invokeExpr= Jimple.v().newStaticInvokeExpr(alertDialog.makeRef(), body.getThisLocal());
		
		Stmt invoketStmt = Jimple.v().newInvokeStmt(invokeExpr);
		
		body.getUnits().insertBefore(invoketStmt, u);
		
//		//add a local
//		Local tmp = Jimple.v().newLocal("tmp", RefType.v("android.app.AlertDialog$Builder"));
//		body.getLocals().add(tmp);
//		System.out.println("yuyifie:" + Scene.v().getField("android.app.AlertDialog$Builder").toString());
////		//add a assign
////		Scene.v().loa
////		Jimple.v().newInstanceFieldRef(base, Scene.v().getField("android.app.AlertDialog$Builder"));
////		body.getUnits().insertBefore(Jimple.v().newAssignStmt(tmp, Jimple.v()., u);
//		//add a virtualinvoke
////		Local base = Jimple.v().newLocal("aaa", invokeExpr.getType());
////		body.getLocals().add(base);
////		//add a assign
////		body.getUnits().insertBefore(Jimple.v().newAssignStmt(base, invokeExpr), u);
	}
	
	private void replaceStatementByLog(Unit u, Body body)
	{
		
		SootMethod sm = Scene.v().getMethod("<android.widget.Toast: android.widget.Toast makeText"
				+ "(android.content.Context,java.lang.CharSequence,int)>");
		System.out.println("--------------> " + sm.getSignature());
		
		System.out.println("-------------->" + new ThisRef(RefType.v("de.ecspride.RV2013")).toString());
		
		SootClass c = Scene.v().getSootClass("de.ecspride.RV2013");
		System.out.println("--------------> " + c.getClass().getName());
        
        
//        Value aaa = body.getUseBoxes().get(0).getValue();
//        SootClass bbb =  Scene.v().getSootClass("android.content.Context");
	    
		StaticInvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(sm.makeRef(),
				body.getThisLocal(),
				StringConstant.v("新年好aaas"),
				IntConstant.v(0));
		System.out.println(invokeExpr.toString());
		Unit generated = Jimple.v().newInvokeStmt(invokeExpr);
		//body.getUnits().insertBefore(generated, u);
		
		//add a 
		Local base = Jimple.v().newLocal("aaa", invokeExpr.getType());
		body.getLocals().add(base);
		//add a assign
		body.getUnits().insertBefore(Jimple.v().newAssignStmt(base, invokeExpr), u);
//        body.getUnits().insertAfter(Jimple.v().newIdentityStmt(arg, 
//		Jimple.v().newThisRef(RefType.v("de.ecspride.RV2013"))),
//		body.getUnits().getFirst());
		
		
		SootMethod sm1 = Scene.v().getMethod("<android.widget.Toast: void show()>");
		body.getUnits().insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newVirtualInvokeExpr(base, sm1.makeRef())), u);
		
//        units.insertBefore(Jimple.v().newInvokeStmt(
//                Jimple.v().newVirtualInvokeExpr(tmpRef, toCall.makeRef(), tmpString)), u);
		
//		SootMethod sm = Scene.v().getMethod("<android.util.Log: int i(java.lang.String,java.lang.String)>");
//		Value logType = StringConstant.v("INFO");
//		Value logMessage = StringConstant.v("yuyifei 2015hhhh: replaced log information");
//		
//		StaticInvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(sm.makeRef(), logType, logMessage);
//		Unit generated = Jimple.v().newInvokeStmt(invokeExpr);
//		
//		body.getUnits().insertAfter(generated, u);
//		
//		body.getUnits().remove(u);
	}
	
}

