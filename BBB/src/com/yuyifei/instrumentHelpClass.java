package com.yuyifei;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;

public class instrumentHelpClass {
	public static void myPrintStack()
	{
		StackTraceElement[]  stackTraceElements = Thread.currentThread().getStackTrace();
//		for (int i=0; i<stackTraceElements.length; ++i)
//		{
//			StackTraceElement st = stackTraceElements[i];
//			System.out.println("ClassName:" + st.getClassName() +"+++"
//					+"FileName:" + st.getFileName() + "+++"
//					+"LineNumber:" + st.getLineNumber() + "+++"
//					+"MethodName:" + st.getMethodName() + "+++");
//		}
		
	    for (int i = 0, n = stackTraceElements.length; i < n; i++) {       
	        System.err.println(stackTraceElements[i].getFileName()
	            + ":" + stackTraceElements[i].getLineNumber() 
	            + ">> "
	            + stackTraceElements[i].getMethodName() + "()");
	    }
	}
	
	public static void printObject(Object obj)
	{
		System.out.println("-----> object:" + obj.toString());
	}
	
	public static void instrumentAlertDialog(Context context)
	{
		CharSequence title = "余奕飞";
		CharSequence message = "有人偷发短信";
		CharSequence button = "确定";
		OnClickListener listen = null;
		
		new AlertDialog.Builder(context)
		.setTitle(title)
		.setMessage(message)
		.setPositiveButton(button, listen)
		.show();
	}
}
