package com.instrument;

import java.util.Iterator;
import java.util.Map;

import soot.ArrayType;
import soot.Body;
import soot.BodyTransformer;
import soot.Local;
import soot.PatchingChain;
import soot.RefType;
import soot.Scene;
import soot.SootClass;
import soot.SootMethod;
import soot.SootMethodRef;
import soot.Unit;
import soot.Value;
import soot.jimple.AssignStmt;
import soot.jimple.IntConstant;
import soot.jimple.InvokeExpr;
import soot.jimple.InvokeStmt;
import soot.jimple.Jimple;
import soot.jimple.StaticFieldRef;
import soot.jimple.StaticInvokeExpr;
import soot.jimple.Stmt;
import soot.jimple.StringConstant;
import soot.jimple.ThisRef;
import soot.jimple.VirtualInvokeExpr;
import soot.util.Chain;

public class MyBodyTransformer extends BodyTransformer
{
	private static SootClass instrumentClass;
	
	static 
	{
		instrumentClass = Scene.v().loadClassAndSupport("com.util.InstrumentHelpClass");
	}
	
	@Override
	protected void internalTransform(Body body, String phaseName, Map<String, String> arg2) 
	{
		if (body.getMethod().getDeclaringClass().getName().startsWith("de.ecspride")) 
		{
			PatchingChain<Unit> units = body.getUnits();
			
			if (body.getMethod().getName().contains("sendSms"))
			{
				Iterator<Unit> i = units.snapshotIterator();
				while (i.hasNext())
				{
					Unit u = i.next();
					if(u.toString().contains("println"))
					{
						//removeStatement(u, body);
						//instrumentSystemOutPrintln(u, body, "---------> yuyifei:Hello");
						//instrumentToast(u, body);
						//instrumentAlertDialog(u, body);
						//printStack(u, body);
						printParameter(u, body);
						
						if(u instanceof AssignStmt)
						{
							
						}
					}
				}
			}
		}
	}
	
	private void printParameter(Unit u, Body body)
	{
		String info = "";
		int i = 0;
				
		Value param = body.getParameterRefs().get(0);
		System.out.println("------->" + body.getParameterLocal(0).getType().toString());
		
		SootMethod printMethod =  instrumentClass.getMethodByName("printObject");
		System.out.println("---------->" + printMethod.getSignature());
		
		Iterator<Value> params = body.getParameterRefs().iterator();
		while(params.hasNext())
		{
			info = info + "arg" + i + ":";
			info += params.next().getType().toString();
			info += "\n";
			++i;
		}
		
		System.out.println("----------->" + info);
		
		instrumentSystemOutPrintln(u, body, info);
		
//		StaticInvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(printMethod.makeRef(), body.getParameterLocal(0));
//		
//		Unit generated = Jimple.v().newInvokeStmt(invokeExpr);
//		
//		body.getUnits().insertBefore(generated, u);
	}
	
	//print stack trace
	private void printStack(Unit u, Body body)
	{
		System.out.println("+++++++++++++ yuyifeiaa:" + instrumentClass.getMethodCount());
		
		SootMethod stackMethod =  instrumentClass.getMethod("void myPrintStack()");
		// 1. make invoke expression of MyCounter.report()
		InvokeExpr printExpr= Jimple.v().newStaticInvokeExpr(stackMethod.makeRef());

		// 2. then, make a invoke statement
		Stmt printStmt = Jimple.v().newInvokeStmt(printExpr);

		// 3. insert new statement into the chain
		//    (we are mutating the unit chain).
		body.getUnits().insertBefore(printStmt, u);
	}
	
	//instrument a alert dialog with a instrument help class
	private void instrumentAlertDialog(Unit u, Body body)
	{
		SootMethod alertDialog = instrumentClass.getMethodByName("instrumentAlertDialog");
		
		System.out.println("------->" + alertDialog.getSignature());//debug it
		
		InvokeExpr invokeExpr= Jimple.v().newStaticInvokeExpr(alertDialog.makeRef(), body.getThisLocal());
		
		Stmt invoketStmt = Jimple.v().newInvokeStmt(invokeExpr);
		
		body.getUnits().insertBefore(invoketStmt, u);
	}
	
	//instrument a toast
	private void instrumentToast(Unit u, Body body)
	{
		//get a toast method
		SootMethod sm = Scene.v().getMethod("<android.widget.Toast: android.widget.Toast makeText"
				+ "(android.content.Context,java.lang.CharSequence,int)>");
	
		System.out.println("--------------> " + sm.getSignature());	//debug it
	    
		StaticInvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(sm.makeRef(),
														body.getThisLocal(),//get this parameter
														StringConstant.v("新年好aaas"),
														IntConstant.v(0));
		
		System.out.println(invokeExpr.toString());//debug it
		
		//creat a local
		Local base = Jimple.v().newLocal("tmpRef", invokeExpr.getType());
		body.getLocals().add(base);
		//add a assign
		body.getUnits().insertBefore(Jimple.v().newAssignStmt(base, invokeExpr), u);
		
		SootMethod smShow = Scene.v().getMethod("<android.widget.Toast: void show()>");
		body.getUnits().insertBefore(Jimple.v().newInvokeStmt(Jimple.v().newVirtualInvokeExpr(base, smShow.makeRef())), u);
	}
	
	//instrument System.out.println
	private void  instrumentSystemOutPrintln(Unit u, Body body, String info)
	{
		PatchingChain<Unit> units = body.getUnits();
		Chain<Local> locals = body.getLocals();
		
        // Add locals, java.io.printStream tmpRef
        Local tmpRef = Jimple.v().newLocal("tmpRef", RefType.v("java.io.PrintStream"));
        locals.add(tmpRef);
        
//        units.add(Jimple.v().newAssignStmt(tmpRef, Jimple.v().newStaticFieldRef(
//            Scene.v().getField("<java.lang.System: java.io.PrintStream out>").makeRef())));
        //add a assign: "tmpRef = java.lang.System.out"
        {
        	//get a static field ref
        	StaticFieldRef field = Jimple.v().newStaticFieldRef(Scene.v()
        			.getField("<java.lang.System: java.io.PrintStream out>").makeRef());
        	//create a newAssignStmt
        	AssignStmt assign = Jimple.v().newAssignStmt(tmpRef, field);
        	//instrument a assign statement
        	units.insertBefore(assign, u);
        }
		
        // insert "tmpRef.println(info)"
        {
            SootMethod toCall = Scene.v().getMethod("<java.io.PrintStream: void println(java.lang.String)>");
            System.out.println("----->" + toCall.getSignature());
            
            // create a invokeExpr
            VirtualInvokeExpr invokeExpr = Jimple.v().newVirtualInvokeExpr(tmpRef, 
														        		toCall.makeRef(),
														        		StringConstant.v(info));
            //create a invokeStmt
            InvokeStmt invokeStmt = Jimple.v().newInvokeStmt(invokeExpr);
            //instrument a invokeStmt
            units.insertBefore(invokeStmt, u);
        }      
	}

	//remove a unit
	private void removeStatement(Unit u, Body body)
	{
		body.getUnits().remove(u);
	}
	
	//replace LOG information
	private void replaceStatementByLog(Unit u, Body body){
		SootMethod sm = Scene.v().getMethod("<android.util.Log: int i(java.lang.String,java.lang.String)>");
		Value logType = StringConstant.v("INFO-YUYIFEI");
		Value logMessage = StringConstant.v("yuyifei: replaced log information");
		
		StaticInvokeExpr invokeExpr = Jimple.v().newStaticInvokeExpr(sm.makeRef(), logType, logMessage);
		Unit generated = Jimple.v().newInvokeStmt(invokeExpr);
		
		body.getUnits().insertAfter(generated, u);
		
		body.getUnits().remove(u);
	}
}
