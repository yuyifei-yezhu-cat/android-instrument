package com.instrument;

import java.util.Collections;

import soot.G;
import soot.PackManager;
import soot.Scene;
import soot.SootClass;
import soot.Transform;
import soot.options.Options;

public class Main {
	private final static String androidJARPath = "/home/yuyifei/Desktop/android-jar/android-platforms-master";
	private final static String androidJAR = "/home/yuyifei/Desktop/android-jar/android-platforms-master/android-17/android.jar";
	//private final static String apkPath = "/home/yuyifei/Desktop/githhub-paper/android-instrumentation-tutorial/app-example/RV2013/bin/RV2013.apk";
	private final static String apkPath = "/home/yuyifei/Desktop/instrument-framework/framework.jar";
	private final static String jdkPath = "/opt/jdk1.7.0_67/jre/lib/rt.jar";  
	
	public static void main (String[] args)
	{
		G.reset();
		
		String[] args1 = {
				//"-cp",
				//"/home/yuyifei/Downloads/yuyifei/BBB/bin",
				//":/home/yuyifei/Downloads/yuyifei/BBB/bin/com/yuyifei.MyPrint.class",
				//"-android-jars",
				//"/home/yuyifei/Desktop/android-jar/android-platforms-master",
				"-allow-phantom-refs",
				//"-ire",
				"-process-dir",
				//"/home/yuyifei/Desktop/apk-test/SkeletonActivity.apk"
				apkPath
		};
		
		Options.v().set_soot_classpath(jdkPath + ":" + androidJAR + ":./src:" + apkPath);
		Options.v().set_android_jars(androidJARPath);
		Options.v().set_force_android_jar(androidJAR);
		//Options.v().set_allow_phantom_refs(true);
		Options.v().set_src_prec(Options.src_prec_apk);
		//Options.v().set_process_dir(Collections.singletonList(apkPath));
		
		//Options.v().set_output_format(Options.output_format_dex);
		Options.v().set_output_format(Options.output_format_jimple);
		
        // resolve the PrintStream and System soot-classes
        Scene.v().addBasicClass("java.io.PrintStream",SootClass.SIGNATURES);
        Scene.v().addBasicClass("java.lang.System",SootClass.SIGNATURES);
        Scene.v().addBasicClass("android.widget.Toast", SootClass.SIGNATURES);
        //Scene.v().addBasicClass("de.ecspride.RV2013", SootClass.SIGNATURES);
        Scene.v().addBasicClass("com.yuyifei.MyPrint", SootClass.SIGNATURES);
        Scene.v().addBasicClass("android.view.View", SootClass.SIGNATURES);
        Scene.v().addBasicClass("com.yuyifei.instrumentHelpClass", SootClass.SIGNATURES);
        
        PackManager.v().getPack("jtp").add(new Transform("jtp.myInstrumenter", new MyBodyTransformer()));
        
        soot.Main.main(args1);
	}
}
